
# MVPTest
The app allows you to find a GitHub user and to see the list of his followers and followings, you can tap on any user from the list and see his profile.

The goal of this assessment is to build a basic iOS application which demonstrates common tasks such as fetching data, parsing model entities from JSON, UI, and navigation.

3rd Party Libraries:
None.

App Architecture:
The project includes iOS app target. The architecture design pattern used for the application is MVP-R (Model-View-Presenter-Router).

iOS:
The iOS version of the app follows core OOP design principles. The Model-View-Presenter-Router (MVP-R) design pattern used modern app development practices including protocol oriented development with single responsibility, encapsulation principles, dependency injection, xib files and Auto Layout. In the project Singleton pattern was avoided. This approach potentially allows to cover the business logic code with unit tests.
Brief description of MVP-R components.

Model:
DTO-models are of type Codable, they contain information fetched from backend. Structure and properties of the DTO-models will be equal to responses from the backend  in most of the cases. Later these models are transformed to UIModels, which are plain objects, before they will be shown in the UI to users.

View:
The view component shows information in UI. In the app it is an instance of UIViewController. Also, to make sure that UIViewController inherits the common logic & protocols, UIViewController should inherit from BaseController.  In MVP-R, Presenter holds the View with a weak reference and View holds a strong reference to Presenter via the protocol. This approach helps us to make sure, that when View (UIViewController) is deallocated, Presenter will be deallocated as well.

Presenter:
The presenter is responsible for business logic handling. The presenter fetches data from the backend, converts it using Adapter, and passes it to the View (UIViewController). It will have a reference to:
	•	An appropriate APIManager to make API calls.
	•	Data Adapter to convert data from DTOModel to UIModel.
	•	A delegate, that will handle all action, out of Presenter's scope. For example - navigation. Usually, a Router will conform to Presenter's delegate protocol and will be injected as a dependency.


Router:
The Router is responsible for the navigation within the application. It keeps a reference to an instance of UINavigationController. There is a separate Router instance for each flow in the app. When a screen  e.g “MainScreen” needs to be shown, a Router creates the following: MainScreenController, MainScreenPresenter. After, it passes all dependencies to MainScreenPresenter (MainScreenController) and himself as MainScreenPresenterDelegateProtocol. Finally, when MainScreenPresenter is done with logic in it, or user clicks to proceed to the next screen, MainScreenPresenter will deliver this event back to a Router. Router, as a result, will create new classes for the next screen and will push it (Like it was with MainScreenController). It is all done in order to encapsulate the logic of navigation between screens into a Router, and to make sure a Presenter is not aware about other Presenters.
Additional components.

API class:
This component is responsible for making API calls, and it would return an appropriate parsed Model type.

UIModel:
This is converted/transformed data from Model. Usually, it is Adapter’s responsibility to transform data. However if it happens that Presenter can take care of this logic as well. The main idea here is to encapsulate data transformation logic in the Adapter, and let UIViewController only to show the final result in UI. This way guarantees us UIViewController does not take care of any business logic but just shows UIModels in the UI.

Adapter:
This component is only responsible for transforming DTOModel to UIModel.


String Constants:
Constants are defined using enums and static members to avoid keeping constants in the global namespace.
