//
//  UsersApiClientTest.swift
//  UsersApiClientTest
//
//  Created by Yurii on 2021/12/10.
//

import XCTest
@testable import MVPTest

class UsersApiClientTest: XCTestCase {
    var networkSession: URLSessionMock!
    var sut: UserAPIClient?
    var model: UserModelDTO?
    var error: NSError?
    
    override func setUp() {
        super.setUp()
        networkSession = URLSessionMock.mockedSession(with: "getUser", error: nil)
        guard let baseUrl = URL(string: Constants.API.baseURL) else { return }
        
        sut = UserAPIClient(baseUrl: baseUrl, sessionManager: networkSession)
    }
    
    private func makeGetUserCall() {
        sut?.getUserModel(with: "YuriiOhi", success: { (model) in
            self.model = model
        }, failure: { (error) in
            self.error = error as NSError?
        })
    }
    
    func testGetUserSuccess() {
        networkSession.error = nil
        makeGetUserCall()
        
        XCTAssertNil(error)
        XCTAssertEqual(model?.login, "YuriiOhi")
    }
    
    func testGetUserFailure() {
        let error = NSError(domain: Constants.Errors.apiDomain,
                            code: Constants.Errors.defaultErrorCode,
                            userInfo: nil)
        
        networkSession.error = error
        networkSession.data = nil
        makeGetUserCall()
        
        XCTAssertNil(model)
        XCTAssertNotNil(error)
    }
}
