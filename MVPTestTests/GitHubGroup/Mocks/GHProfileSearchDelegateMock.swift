//
//  GHProfileSearchDelegateMock.swift
//  MVPTest
//
//  Created by Yurii on 2021/12/7.
//

import Foundation

@testable import MVPTest

class GHProfileSearchDelegateMock: GHProfileSearchPresenterDelegate {
    
    private(set) var didSelectUserList = false

    private(set) var userListType: UserDTOModels?
    
    func presenter(_ presenter: GHProfileSearchPresenterProtocol, didSelect userListType: UserDTOModels) {
        didSelectUserList = true
        self.userListType = userListType
    }
}
