//
//  GHProfileSearchControllerMock.swift
//  MVPTest
//
//  Created by Yurii on 2021/12/7.
//

import Foundation

@testable import MVPTest

class GHProfileSearchControllerMock: BaseViewControllerMock, GHProfileSearchViewControllerProtocol {
    
    var presenter: GHProfileSearchPresenterProtocol?
    
    private(set) var rows: [MainScreenRow]?
    private(set) var showRowsMainScreenEventReceived = false
    private(set) var hideSearchItemsEventReceived = false

    func show(rows: [MainScreenRow]) {
        self.rows = rows
        self.showRowsMainScreenEventReceived = true
    }
    
    func hideSearchItems() {
        hideSearchItemsEventReceived = true
    }
}
