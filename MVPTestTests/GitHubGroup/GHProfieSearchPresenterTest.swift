//
//  GHProfieSearchPresenterTest.swift
//  MVPTestTests
//
//  Created by Yurii on 2021/12/11.
//

import XCTest
@testable import MVPTest
class GHProfieSearchPresenterTest: XCTestCase {
    private(set) var sut: GHProfileSearchPresenter?
    private(set) var delegate: GHProfileSearchDelegateMock!
    private(set) var controller: GHProfileSearchControllerMock!
    private(set) var userAPIClient: UserAPIClientProtocolMock!
    private(set) var userListAPIClient: UserListAPIClientProtocolMock!
    private var state: State = .profileWithSearch
    private var userNameMock = UserModelDTO.mockedOctoCat.login
    
    override func setUp() {
        super.setUp()
        delegate = GHProfileSearchDelegateMock()
        controller = GHProfileSearchControllerMock()
        userAPIClient = UserAPIClientProtocolMock()
        userListAPIClient = UserListAPIClientProtocolMock()
        sut = GHProfileSearchPresenter(controller: controller,
                                       delegate: delegate,
                                       profileSearchAPI: userAPIClient,
                                       listSearchAPI: userListAPIClient,
                                       state: .profileWithSearch)
    }
    
    func testOnViewDidLoad() {
        userAPIClient.userModel = UserModelDTO.mockedOctoCat

        sut?.onViewDidLoad()
    }
    
    func testSearchUser() {
        userAPIClient.userModel = UserModelDTO.mockedOctoCat
        sut?.onViewDidLoad()
        sut?.searchUser(with: userNameMock)
        
        waitFor { self.controller.showRowsMainScreenEventReceived == true }
        XCTAssertTrue(controller.showLoadingControllerEventReceived)
        XCTAssertTrue(controller.hideLoadingControllerEventReceived)
        XCTAssertTrue(controller.showRowsMainScreenEventReceived)
        XCTAssertEqual(controller.rows?.count, 5)
        XCTAssertTrue(controller.setTitleEventReceived)
        XCTAssertEqual(controller.navBarTitle, Constants.ModuleTitles.ghProfileSearch)
        XCTAssertEqual(self.userNameMock, UserModelDTO.mockedOctoCat.login)
    }
}

extension XCTestCase {
    func waitFor(_ timeout: TimeInterval = 5.0,
                 failureMessage: String = "waitFor condition wasn't met within the time limit",
                 file: StaticString = #file,
                 line: UInt = #line,
                 condition: @escaping () -> Bool) {
        let limit = Date(timeIntervalSinceNow: timeout)

        while !condition() && limit > Date() {
            let next = Date(timeIntervalSinceNow: 0.2)
            RunLoop.current.run(mode: RunLoop.Mode.default, before: next)
        }

        XCTAssert(condition(), failureMessage, file: file, line: line)
    }

    func xctWait(seconds: TimeInterval) {
        let exp = expectation(description: "Test after \(seconds) seconds")
        _ = XCTWaiter.wait(for: [exp], timeout: seconds)
    }
}
