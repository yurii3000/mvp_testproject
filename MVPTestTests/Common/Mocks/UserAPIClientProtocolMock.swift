//
//  UsersAPIClientMock.swift
//  MVPTestTests
//
//  Created by Yurii on 2021/12/12.
//

import Foundation
@testable import MVPTest

class UserAPIClientProtocolMock: UserAPIClientProtocol {
    
    var userModel: UserModelDTO?
    var error: NSError?
    
    private(set) var getUserEventReceived = false
    
    func getUserModel(with userName: String, success: APIUserSuccessCallback?, failure: APIFailureCallBack?) {
        getUserEventReceived = true
        
        if let mockedModel = userModel {
            success?(mockedModel)
        } else if let error = error {
            failure?(error)
        }
    }
}
