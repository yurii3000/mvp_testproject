//
//  UserListAPIClientMock.swift
//  MVPTestTests
//
//  Created by Yurii on 2021/12/12.
//

import Foundation
@testable import MVPTest

class UserListAPIClientProtocolMock: UsersListAPIClientProtocol {
    
    private(set) var userListModel: Bool = false
    
    func getUsersListModel(with listPath: String, successArray: APIArraySuccessCallback?, failure: APIFailureCallBack?) {
        self.userListModel = true
    }
}

