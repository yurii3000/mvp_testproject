//
//  BaseViewControllerMock.swift
//  MVPTest
//
//  Created by Yurii on 2021/12/7.
//

import Foundation
 
@testable import MVPTest

class BaseViewControllerMock: BaseViewControllerProtocol {
  
    var loadingController: LoadingViewController?
    
    private(set) var showErrorEventReceived = false

    private(set) var showLoadingControllerEventReceived = false
    private(set) var hideLoadingControllerEventReceived = false
    
    private(set) var setTitleEventReceived = false
    
    private(set) var errorMessage: String?
    private(set) var errorTitle: String?
    private(set) var error: NSError?
    private(set) var navBarTitle: String?
    
    func set(title: String) {
        navBarTitle = title
        setTitleEventReceived = true
    }
    
    func show(error: NSError) {
        showErrorEventReceived = true
        self.error = error
    }
    
    func show(message: String, title: String?) {
        showErrorEventReceived = true
        errorTitle = title
        errorMessage = message
    }
    
    func showLoadingController(aboveNavBar: Bool) {
        showLoadingControllerEventReceived = aboveNavBar
    }
    
    func hideLoadingController() {
        hideLoadingControllerEventReceived = true
    }
}

