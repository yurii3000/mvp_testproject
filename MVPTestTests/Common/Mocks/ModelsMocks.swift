//
//  ModelsMocks.swift
//  MVPTestTests
//
//  Created by Yurii on 2021/12/15.
//

import Foundation
@testable import MVPTest

extension UserModelDTO {
    static var mockedYurii: UserModelDTO {
        return UserModelDTO(login: "YuriiOhi",
                            id: 60636490,
                            nodeID: "MDQ6VXNlcjYwNjM2NDkw",
                            avatarURL: "https://avatars.githubusercontent.com/u/60636490?v=4",
                            gravatarID: "",
                            url: "https://api.github.com/users/YuriiOhi",
                            htmlURL: "https://github.com/YuriiOhi",
                            followersURL: "https://api.github.com/users/YuriiOhi/followers",
                            followingURL: "https://api.github.com/users/YuriiOhi/following{/other_user}",
                            gistsURL: "https://api.github.com/users/YuriiOhi/gists{/gist_id}",
                            starredURL: "https://api.github.com/users/YuriiOhi/starred{/owner}{/repo}",
                            subscriptionsURL: "https://api.github.com/users/YuriiOhi/subscriptions",
                            organizationsURL: "https://api.github.com/users/YuriiOhi/orgs",
                            reposURL: "https://api.github.com/users/YuriiOhi/repos",
                            eventsURL: "https://api.github.com/users/YuriiOhi/events{/privacy}",
                            receivedEventsURL: "https://api.github.com/users/YuriiOhi/received_events",
                            type: "User",
                            siteAdmin: false,
                            name: nil, company: nil,
                            blog: "",
                            location: nil, email: nil, hireable: nil, bio: nil, twitterUsername: nil,
                            publicRepos: 1, publicGists: 0, followers: 0, following: 11,
                            createdAt: "2020-02-04T03:19:29Z", updatedAt: "2021-10-27T15:13:12Z")
    }
    
    static var mockedOctoCat: UserModelDTO {
        return UserModelDTO(login: "octocat",
                            id: 583231,
                            nodeID: "DQ6VXNlcjU4MzIzMQ==",
                            avatarURL: "https://avatars.githubusercontent.com/u/583231?v=4",
                            gravatarID: "",
                            url: "https://api.github.com/users/octocat",
                            htmlURL: "https://github.com/octocat",
                            followersURL: "https://api.github.com/users/octocat/followers",
                            followingURL: "https://api.github.com/users/octocat/following{/other_user}",
                            gistsURL: "https://api.github.com/users/octocat/gists{/gist_id}",
                            starredURL: "https://api.github.com/users/octocat/starred{/owner}{/repo}",
                            subscriptionsURL: "https://api.github.com/users/octocat/subscriptions",
                            organizationsURL: "https://api.github.com/users/octocat/orgs",
                            reposURL: "https://api.github.com/users/octocat/repos",
                            eventsURL: "https://api.github.com/users/octocat/events{/privacy}",
                            receivedEventsURL: "https://api.github.com/users/octocat/received_events",
                            type: "User",
                            siteAdmin: false,
                            name: "The Octoat", company: "@github",
                            blog: "https://github.blog",
                            location: "San Franciscol",
                            email: nil, hireable: nil, bio: nil, twitterUsername: nil,
                            publicRepos: 8, publicGists: 8, followers: 4225, following: 9,
                            createdAt: "2011-01-25T18:44:36Z", updatedAt: "2021-12-14T04:39:03Z")
    }
}
