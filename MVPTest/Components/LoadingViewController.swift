//
//  LoadingViewController.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/29.
//

import Foundation
import UIKit

class LoadingViewController: BaseViewController {

    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.color = .red
        activityIndicator.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIAccessibility.post(notification: .layoutChanged, argument: "loading".localized)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}
