//
//  ErrorViewController.swift
//  SpeerTechTest
//
//  Created by Yurii on 2021/9/30.
//

import Foundation
import UIKit

class ErrorViewController: BaseViewController {

    @IBOutlet private weak var errorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorView.backgroundColor = .red
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIAccessibility.post(notification: .layoutChanged, argument: "loading".localized)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}
