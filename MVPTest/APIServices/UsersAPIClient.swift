//
//  UsersAPIClient.swift
//  MVPTest
//
//  Created by Yurii on 2021/11/1.
//

import Foundation

typealias APIUserSuccessCallback = (_ user: UserModelDTO) -> Void

protocol UserAPIClientProtocol {
    func getUserModel(with userName: String, success: APIUserSuccessCallback?, failure: APIFailureCallBack?)
}

class UserAPIClient: APIManager, UserAPIClientProtocol {
    func getUserModel(with userName: String, success: APIUserSuccessCallback?, failure: APIFailureCallBack?) {
        self.get(with: userName, params: nil, success: { (response) in
            let model: UserModelDTO = try self.decodeJSONOrThrow(response)
            
            success?(model)
        }, failure: { (error) in
            failure?(error)
        })
    }
}
