//
//  APIManager.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import Foundation

typealias APIDictCallBack = ([String: Any]) throws -> Void
typealias APIArrayCallBack = ([[String: Any]]) throws -> Void
typealias APIFailureCallBack = (Error?) -> Void

enum APISuccessType {
    case dict(APIDictCallBack?)
    case array(APIArrayCallBack?)
}

protocol APIManagerProtocol {
    func get(with path: String,
             params: [String: String]? ,
             success: @escaping APIDictCallBack,
             failure: @escaping APIFailureCallBack)
    
    func get(with path: String,
             params: [String: String]?,
             successArray: @escaping APIArrayCallBack,
             failure: @escaping APIFailureCallBack)
}

class APIManager: NSObject, APIManagerProtocol {
    private let baseUrl: URL!
    private let sessionManager: URLSessionProtocol
    
    convenience override init() {
        guard let baseURL = URL(string: Constants.API.baseURL + Constants.API.usersPathURL) else {
            fatalError("APIManager: Url initialization issue")
        }
        
        self.init(baseUrl: baseURL, sessionManager: URLSession.default)
    }
    
    required init(baseUrl: URL, sessionManager: URLSessionProtocol = URLSession.default) {
        self.baseUrl = baseUrl
        self.sessionManager = sessionManager
    }
    
    // MARK: - Success block is Dict
    func get(with path: String,
             params: [String : String]?,
             success: @escaping APIDictCallBack,
             failure: @escaping APIFailureCallBack) {
        let urlRequest = self.createGetRequest(path: path, params: defaultHeaders(), headerParams: defaultHeaders())
        self.dataTask(with: urlRequest, successType: .dict(success), failure: failure).resume()
    }
    
    // MARK: - Success block is Array
    func get(with path: String,
             params: [String : String]?,
             successArray: @escaping APIArrayCallBack,
             failure: @escaping APIFailureCallBack) {
        let urlRequest = self.createGetRequest(path: path, params: defaultHeaders(), headerParams: defaultHeaders())
        self.dataTask(with: urlRequest, successType: .array(successArray), failure: failure).resume()
    }
}

extension APIManager {
    func createGetRequest(path: String, params: [String: String]?, headerParams: [String: String]?) -> URLRequest {
        let urlComponents = self.urlComponents(with: path)
        urlComponents.queryItems = params?.map { (key, value) -> URLQueryItem in URLQueryItem(name: key, value: value) }
        
        var urlRequest = URLRequest(url: urlComponents.url!)
        headerParams?.forEach { header in urlRequest.setValue(header.value, forHTTPHeaderField: header.key) }
        return urlRequest
    }
    
    func urlComponents(with path: String) -> NSURLComponents {
        let path = "/" + path
        let urlComponents = NSURLComponents()
        urlComponents.scheme = baseUrl.scheme
        urlComponents.host = baseUrl.host
        urlComponents.path = baseUrl.path + path
        
        return urlComponents
    }
    
    func decodeJSONOrThrow<T: Decodable>(_ response: Any?) throws -> T {
        let data = try JSONSerialization.data(withJSONObject: response as Any, options: [])
        let decoder = JSONDecoder()
        let model = try! decoder.decode(T.self, from: data)
        
        return model
    }
    
    func validate(data: Data?, response: URLResponse?, error: Error?) -> NSError? {
        if let error = error {
            return error as NSError
        } else if let response = response as? HTTPURLResponse, response.statusCode != 200 {
            let nserror = NSError(domain: APIManager.className, code: response.statusCode, userInfo: data?.user)
            print(nserror)
            return nserror
        }
        
        return nil
    }
    
    private func dataTask(with urlRequest: URLRequest,
                          successType: APISuccessType,
                          failure: APIFailureCallBack?) -> URLSessionDataTask {
        return sessionManager.dataTask(with: urlRequest) { (data: Data?, urlResponse: URLResponse?, error: Error?) in
            if let error = self.validate(data: data, response: urlResponse, error: error) {
                failure?(error)
                return
            }
            do {
                switch successType {
                case let .array(successClosure):
                    guard let data = data, let array = data.array else {
                        try successClosure?([])
                        return
                    }
                    
                    try successClosure?(array)
                case let .dict(successClosure):
                    guard let data = data, let user = data.user else {
                        try successClosure?([:])
                        return
                    }
                    
                    try successClosure?(user)
                }
            } catch {
                failure?(NSError())
            }
        }
    }
}

private extension APIManager {
    func parseErrorCode(with response: HTTPURLResponse) -> Error? {
        let statusCode = response.statusCode
        
        switch statusCode {
        case 200:
            return nil
        case 404:
            return NSError.notFoundError
        default:
            return NSError(domain: Constants.API.github, code: statusCode, userInfo: nil)
        }
    }
    
    func handleError(error: Error?, failure: ((NSError) -> Void)) {
        guard let error = error as NSError? else {
            failure(NSError.genericError)
            return
        }
        
        failure(error)
    }
    
    func defaultHeaders() -> [String: String] {
        return [
            Constants.API.contentTypeHeader: Constants.API.contentTypeJSON,
            Constants.API.headerFieldAccept: Constants.API.contentForAccept,
            Constants.API.headerFieldAcceptEncoding: Constants.API.contentForAcceptEncoding,
            Constants.API.headerFieldConnection: Constants.API.contentForConnection
        ]
    }
}
