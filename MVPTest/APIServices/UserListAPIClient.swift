//
//  UserListAPIClient.swift
//  MVPTest
//
//  Created by Yurii on 2021/11/1.
//

import Foundation

typealias APIArraySuccessCallback = (_ user: UserDTOModels) -> Void

protocol UsersListAPIClientProtocol {
    func getUsersListModel(with listPath: String, successArray: APIArraySuccessCallback?, failure: APIFailureCallBack?)
}

class UsersListAPIClient: APIManager, UsersListAPIClientProtocol {
    func getUsersListModel(with listPath: String, successArray: APIArraySuccessCallback?, failure: APIFailureCallBack?) {
        self.get(with: listPath, params: nil, successArray: { (response) in
            let models: UserDTOModels = try self.decodeJSONOrThrow(response)
            
            successArray?(models)
        }, failure: { (error) in
            failure?(error)
        })
    }
}
