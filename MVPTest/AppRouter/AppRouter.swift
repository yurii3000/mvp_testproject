//
//  AppRouter.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import Foundation
import UIKit

class AppRouter {
    private let navController: UINavigationController
    private var ghRouter: GHProfileSearchRouter?
    
    init(navController: UINavigationController) {
        self.navController = navController
        showInitialFlow()
    }
    
    private func showInitialFlow() {
        ghRouter = GHProfileSearchRouter(navController: self.navController)
    }
}
