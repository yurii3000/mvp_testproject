//
//  Constants.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import Foundation
import UIKit

enum Constants {
    enum API {
        static let contentTypeHeader = "Content-Type"
        static let contentTypeJSON = "application/json"
        static let httpMethodPost = "POST"
        static let httpMethodGet = "GET"
        static let github = "GitHub"
        static let baseURL = "https://api.github.com/"
        static let usersPathURL = "users/"
        static let followersPath = "%@/followers"
        static let followingsPath = "%@/following"
        static let headerFieldAccept = "Vary"
        static let contentForAccept = "Accept"
        static let headerFieldAcceptEncoding = "Accept-Encoding"
        static let contentForAcceptEncoding = "gzip, deflate, br"
        static let headerFieldConnection = "Connection"
        static let contentForConnection = "keep-alive"
    }
        
    enum DateFormats {
        static let main = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
    
    enum Errors {
        static let errCallingGET = "Error: error calling GET"
        static let errNoData = "Error: Did not receive data"
        static let errFailedHTTP = "Error: HTTP request failed"
        static let errConvertToJSON = "Error: Cannot convert data to JSON object"
        static let errJsonToPretty = "Error: Cannot convert JSON object to Pretty JSON data"
        static let errPrintJSON = "Error: Could not print JSON in String"
        static let errJsonToString = "Error: Trying to convert JSON data to string"
        static let err404 = "The operation couldn’t be completed. User not found. 404."
        static let apiDomain = "APIHelper"
        static let defaultErrorCode = 400
    }
    
    enum Button {
        static let title = "Search"
    }
    
    enum TextField {
        static let placeholder = "Enter username"
    }
    
    enum RegularExpression {
        static let regEx = "[A-Z0-9a-z.-]+[A-Z0-9a-z]\\d{0,38}"
    }
    
    enum ModuleTitles {
        static let ghProfileSearch = "Search"
        static let ghUserList = "UserList"
    }
}
