//
//  Extensions.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import Foundation
import UIKit

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

extension NSObject {
    class var className: String {
        return String(describing: self)
    }
}

extension NSError {
    static let notFoundError = NSError(domain: "ghb",
                                       code: -1,
                                       userInfo: [NSLocalizedDescriptionKey: "Not found"])
    static let genericError = NSError(domain: "ghb",
                                      code: -1,
                                      userInfo: [NSLocalizedDescriptionKey: "Unknown issue"])
    static let noDataError = NSError(domain: "ghb",
                                     code: -1,
                                     userInfo: [NSLocalizedDescriptionKey: "No data"])
    static let parseError = NSError(domain: "ghb",
                                    code: -1,
                                    userInfo: [NSLocalizedDescriptionKey: "Parse error"])
}

extension URLSession {
    static var `default`: URLSession {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = TimeInterval(10)
        config.timeoutIntervalForResource = TimeInterval(10)
        return URLSession(configuration: config, delegate: nil, delegateQueue: OperationQueue.main)
    }
}


extension Data {
    var user: [String: Any]? {
        return (try? JSONSerialization.jsonObject(with: self, options: [])) as? [String: Any]
    }
    
    var array: [[String: Any]]? {
        return (try? JSONSerialization.jsonObject(with: self, options: [])) as? [[String: Any]]
    }
    
    static func load(from jsonFile: String, bundle: Bundle = Bundle.main)  -> Data {
        guard let urlString = bundle.path(forResource: jsonFile, ofType: "json"),
            let url = URL(string: urlString) else {
            fatalError("Failed to locate \(jsonFile) in bundle.")
        }

        guard let contentData = FileManager.default.contents(atPath: urlString) else {
            fatalError("Failed to load \(jsonFile) from bundle.")
        }
        
        return contentData
    }
}

