//
//  UserAdapter.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import Foundation
import UIKit

//This component is only responsible for transforming Model to UIModel
protocol UserAdapterProtocol {
    static func convert(_ user: UserModelDTO) -> [MainScreenRow]
}

class UserAdapter: UserAdapterProtocol {
    private static func convert(value: Int?) -> String? {
        guard let value = value else { return nil }
        
        return String(value)
    }
    
    static func convert(_ user: UserModelDTO) -> [MainScreenRow] {
        var rows = [MainScreenRow.header(URL(string: user.avatarURL))]
        
        let content = [user.login, user.name, user.bio, UserAdapter.convert(value: user.followers), UserAdapter.convert(value: user.following)]
        let fieldTypes = FieldUIModel.FieldType.allCases
        
        for (index, item) in content.enumerated() {
            if let item = item {
                let model = FieldUIModel(type: fieldTypes[index], subTitle: item)
                rows.append(.field(model))
            }
        }
        return rows
    }
    
    static func convertArrayToUI(_ models: UserDTOModels) -> [UserUIModel] {
        var userUIModels = [UserUIModel]()
        let last = models.count
        
        for modelIndex in 0..<last {
            userUIModels.append(convertToUI(model: models[modelIndex]))
        }
        
        return userUIModels
    }
}

extension UserAdapter {
    static func convertToUI(model: UserModelDTO) -> UserUIModel {
        
        let uiModel = UserUIModel(userName: model.login,
                                  name: model.name,
                                  avatarUrl: model.avatarURL,
                                  description: model.bio,
                                  followersUrl: model.followersURL,
                                  followingUrl: model.followingURL)
        return uiModel
    }
}
