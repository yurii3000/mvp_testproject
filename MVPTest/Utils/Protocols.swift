//
//  Protocols.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/29.
//

import Foundation

typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void

protocol BasePresenterProtocol: AnyObject {
    func onViewDidLoad()
}

protocol URLSessionProtocol {
    func dataTask(with url: URL, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask
    func dataTask(with request: URLRequest, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask
}

extension URLSession: URLSessionProtocol {}

