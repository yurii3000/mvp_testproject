//
//  MainScreen.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import UIKit

protocol GHProfileSearchViewControllerProtocol: BaseViewControllerProtocol {
    var presenter: GHProfileSearchPresenterProtocol? { get set }
    
    func show(rows: [MainScreenRow])
    func hideSearchItems()
}

final class GHProfileSearchViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchStackView: UIStackView!
    
    var presenter : GHProfileSearchPresenterProtocol?
    var userNamePath: String?
    var rows: [MainScreenRow] = [MainScreenRow]()
    
    @IBAction func searchButton(sender: UIButton) {
        self.userNamePath = searchTextField.text
        
        guard let userNamePath = userNamePath else { return }
        guard isValidUsername(username: userNamePath) else { return }
        
        presenter?.searchUser(with: userNamePath)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureTextfield()
        configureButton()
        hideBackBtnTitle()
        presenter?.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideLoadingController()
    }
}

//MARK: - UITableViewDataSource
extension GHProfileSearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = rows[indexPath.row]
        
        switch model {
        case .header(let url):
            let identifier = String(describing: HeaderCell.self)
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HeaderCell else {
                return UITableViewCell()
            }
            cell.configure(with: url)
            
            return cell
            
        case .field(let model):
            let identifier = String(describing: UserListCell.self)
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? UserListCell else {
                return UITableViewCell()
            }
            let modelType = model.type
            
            if (modelType == .followers || modelType == .following) {
                cell.accessoryType = .disclosureIndicator
            } else {
                cell.accessoryType = .none
            }
            cell.configure(with: model)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
}

//MARK: - UITableViewDelegate
extension GHProfileSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = rows[indexPath.row]
        
        switch model {
        case .field(let model):
            switch model.type {
            case .followers:
                presenter?.onSelectUserList(with: Constants.API.followersPath)
            case .following:
                presenter?.onSelectUserList(with: Constants.API.followingsPath)
            default:
                print(model.type)
            }
        default:
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

//MARK: - Configration of TableView/Textfields/Buttons
extension GHProfileSearchViewController: UITextFieldDelegate {
    func configureTableView() {
        tableView.register(cells: [HeaderCell.self, UserListCell.self])
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
    }
    
    func hideSearchItems() {
        searchStackView.isHidden = true
    }
    
    func configureTextfield() {
        searchTextField.delegate = self
        searchTextField.attributedPlaceholder = NSAttributedString(string: Constants.TextField.placeholder,
                                                                   attributes:  [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    
    func configureButton() {
        searchButton.setTitle(Constants.Button.title, for: .normal)
    }
}

//MARK: - GHProfileSearchViewControllerProtocol
extension GHProfileSearchViewController: GHProfileSearchViewControllerProtocol {
    func show(rows: [MainScreenRow]) {
        self.rows = rows
        tableView.reloadData()
    }
}

private extension GHProfileSearchViewController {
    func isValidUsername(username:String) -> Bool {
        let regEx = Constants.RegularExpression.regEx
        let usernamePred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return usernamePred.evaluate(with: username)
    }
}
