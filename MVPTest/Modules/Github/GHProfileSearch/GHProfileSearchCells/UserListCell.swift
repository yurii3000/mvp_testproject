//
//  UserListCell.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import Foundation
import UIKit

class UserListCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var dividerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dividerView.backgroundColor = .gray
    }
    
    func configure(with model: FieldUIModel) {
        titleLabel.text = model.title
        subTitleLabel.text = model.subTitle
    }
}
