//
//  HeaderCell.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import Foundation
import UIKit

class HeaderCell: UITableViewCell {
    @IBOutlet private var avatarView: UIImageView!
    @IBOutlet private var noImageView: UIView!
    @IBOutlet private var noImageLabel: UILabel!
    @IBOutlet weak var dividerView: UIView!
    
    override func awakeFromNib() {
        noImageView.isHidden = true
        dividerView.backgroundColor = .gray
    }
    
    override func prepareForReuse() {
        noImageView.isHidden = true
    }
    
    func configure(with url: URL?) {
        guard let url  = url else {
            showNoImageView()
            return
        }
        
        avatarView.image(with: url, placeHolderImage: nil, failureClosure: { [weak self] in
            self?.showNoImageView()
        })
    }
    
    private func showNoImageView() {
        noImageView.isHidden = false
    }
}
