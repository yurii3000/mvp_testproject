//
//  GHPresenter.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/29.
//

import Foundation

protocol GHProfileSearchPresenterDelegate: AnyObject {
    func presenter(_ presenter: GHProfileSearchPresenterProtocol, didSelect userListType: UserDTOModels)
}

protocol GHProfileSearchPresenterProtocol: BasePresenterProtocol {
    func searchUser(with query: String)
    func onSelectUserList(with query: String)
}

enum State {
    case onlyProfile(UserModelDTO), profileWithSearch
}

class GHProfileSearchPresenter {
    private weak var delegate: GHProfileSearchPresenterDelegate?
    private weak var controller: GHProfileSearchViewControllerProtocol?
    private let userAPIClient: UserAPIClientProtocol?
    private let listAPIClient: UsersListAPIClientProtocol?
    private var user: UserModelDTO?
    private var userName: String?
    private var state: State
    
    init(controller: GHProfileSearchViewControllerProtocol,
         delegate: GHProfileSearchPresenterDelegate,
         profileSearchAPI: UserAPIClientProtocol = UserAPIClient(),
         listSearchAPI: UsersListAPIClientProtocol = UsersListAPIClient(),
         state: State = .profileWithSearch) {
        self.controller = controller
        self.delegate = delegate
        self.userAPIClient = profileSearchAPI
        self.listAPIClient = listSearchAPI
        self.state = state
        
        self.controller?.presenter = self
    }
}

//MARK: - GHProfileSearchPresenterProtocol
extension GHProfileSearchPresenter: GHProfileSearchPresenterProtocol {
    func onViewDidLoad() {
        configureUI(with: state)
    }
    
    func searchUser(with query: String) {
        controller?.showLoadingController(aboveNavBar: true)
        self.userName = query
        userAPIClient?.getUserModel(with: query, success: { [weak self] (userModelDTO) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.update(with: userModelDTO)
            }
        }, failure: { [weak self] (error) in
            guard let self = self,
                  let error = error as NSError? else { return }
            
            DispatchQueue.main.async {
                self.handle(error: error)
            }
        })
        
    }
    
    func onSelectUserList(with query: String) {
        guard let userName = self.userName else { return }
        let fullPath = String(format: query, userName)
        
        controller?.showLoadingController(aboveNavBar: true)
        
        listAPIClient?.getUsersListModel(with: fullPath, successArray: { [weak self] (models) in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.delegate?.presenter(self, didSelect: models)
            }
            
        }, failure: { [weak self] (error) in
            guard let self = self,
                  let error = error as NSError? else { return }
            
            DispatchQueue.main.async {
                self.handle(error: error)
            }
        })
    }
    
}

// MARK: - Private methods
private extension GHProfileSearchPresenter {
    func handle(error: NSError) {
        self.controller?.hideLoadingController()
        self.controller?.show(error: error)
    }
    
    func update(with user: UserModelDTO) {
        self.user = user
        let modelsForUI = UserAdapter.convert(user)
        controller?.hideLoadingController()
        controller?.show(rows: modelsForUI)
        controller?.set(title: Constants.ModuleTitles.ghProfileSearch.localized)
    }
    
    func configureUI(with state: State) {
        if case let .onlyProfile(model) = state {
            controller?.hideLoadingController()
            controller?.hideSearchItems()
            update(with: model)
        }
    }
}
