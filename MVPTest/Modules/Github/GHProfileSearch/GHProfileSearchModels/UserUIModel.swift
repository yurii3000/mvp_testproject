//
//  UserModel.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import Foundation
//MARK: - UI UserModel
struct UserUIModel {
    let userName: String
    let name: String?
    let avatarUrl: String
    let description: String?
    let followersUrl: String
    let followingUrl: String
}

//MARK: - UIModel for cells
enum MainScreenRow {
    case header(URL?)
    case field(FieldUIModel)
}

struct FieldUIModel {
    enum FieldType: String, CaseIterable {
        case username = "Username"
        case name = "Name"
        case bio = "Bio"
        case followers = "Followers"
        case following = "Following"
    }
    
    let type: FieldType
    let subTitle: String
    
    var title: String {
        return type.rawValue.localized
    }
}
