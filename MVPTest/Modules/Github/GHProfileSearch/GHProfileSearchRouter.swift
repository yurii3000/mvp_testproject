//
//  GHRouter.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/28.
//

import Foundation
import UIKit

class GHProfileSearchRouter {
    private let navController: UINavigationController
    
    init(navController: UINavigationController) {
        self.navController = navController
        let vc = GHProfileSearchViewController(loadType: .xib)
        let _ = GHProfileSearchPresenter(controller: vc, delegate: self)
        
        navController.viewControllers = [vc]
    }
}

// MARK: - GHProfileSearchPresenterDelegate
extension GHProfileSearchRouter: GHProfileSearchPresenterDelegate {
    func presenter(_ presenter: GHProfileSearchPresenterProtocol, didSelect userListModel: UserDTOModels) {
        presentUserList(with: userListModel)
    }
}

// MARK: - Private Methods
private extension GHProfileSearchRouter {
    func presentUserList(with models: UserDTOModels) {
        let userListViewController = GHUserListViewController(loadType: .xib)
        let _ = GHUserListPresenter(controller: userListViewController, delegate: self, usersList: models)
        
        self.navController.pushViewController(userListViewController, animated: true)
    }
}

// MARK: - GHUserListPresenterDelegate
extension GHProfileSearchRouter: GHUserListPresenterDelegate {
    func presenter(_ presenter: GHUserListPresenterProtocol, didSelect profileDTOModel: UserModelDTO) {
        let vc = GHProfileSearchViewController.init(loadType: .xib)
        let _ = GHProfileSearchPresenter(controller: vc, delegate: self, state: .onlyProfile(profileDTOModel))
        
        self.navController.pushViewController(vc, animated: true)
    }
}
