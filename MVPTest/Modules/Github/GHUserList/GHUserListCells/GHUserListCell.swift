//
//  GHUserListCell.swift
//  MVPTest
//
//  Created by Yurii on 2021/10/4.
//

import Foundation
import UIKit

class GHUserListCell: UITableViewCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var noImageView: UIView!
    @IBOutlet weak var bioLabel: UILabel!
    
    func cellSetup(with model: UserUIModel) {
        self.userName.text = model.userName
        self.name.text = model.name
        configure(with: URL(string: model.avatarUrl))
        self.bioLabel.text = model.description
    }
    
    override func awakeFromNib() {
        noImageView.isHidden = true
    }
    
    override func prepareForReuse() {
        noImageView.isHidden = true
    }
    
    func configure(with url: URL?) {
        guard let url  = url else {
            showNoImageView()
            return
        }
        
        avatarView.image(with: url, placeHolderImage: nil, failureClosure: { [weak self] in
            self?.showNoImageView()
        })
    }
    
    private func showNoImageView() {
        noImageView.isHidden = false
    }
}
