//
//  GHUserListViewController.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/30.
//

import Foundation
import UIKit
protocol GHUserListViewControllerProtocol: BaseViewControllerProtocol {
    var presenter: GHUserListPresenterProtocol? { get set }
    
    func show(rows: [UserUIModel])
}

final class GHUserListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: GHUserListPresenterProtocol?
    var rows = [UserUIModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        presenter?.onViewDidLoad()
        hideLoadingController()
    }
}

//MARK: - GHUserListViewControllerProtocol
extension GHUserListViewController: GHUserListViewControllerProtocol {
    func show(rows: [UserUIModel]) {
        self.rows = rows
        tableView.reloadData()
    }
}

extension GHUserListViewController {
    func configureTableView() {
        tableView.register(cells: [GHUserListCell.self])
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
    }
}

//MARK: - UITableViewDataSource
extension GHUserListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = rows[indexPath.row]
        let identifier = String(describing: GHUserListCell.self)
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? GHUserListCell else {
            return UITableViewCell()
        }
        
        cell.cellSetup(with: model)
        
        return cell
    }
}

//MARK: - UITableViewDelegate
extension GHUserListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = rows[indexPath.row]
        
        presenter?.onSelectProfile(with: model)
    }
}
