//
//  GHUserListPresenter.swift
//  MVPTest
//
//  Created by Yurii on 2021/9/30.
//

import Foundation

protocol GHUserListPresenterDelegate: AnyObject {
    func presenter(_ presenter: GHUserListPresenterProtocol, didSelect profileDTOModel: UserModelDTO)
}

protocol GHUserListPresenterProtocol: BasePresenterProtocol {
    func onViewDidLoad()
    func showList()
    func onSelectProfile(with profileModel: UserUIModel)
}

class GHUserListPresenter {
    private weak var delegate: GHUserListPresenterDelegate?
    private weak var controller: GHUserListViewControllerProtocol?
    private let apiManager: APIManager?
    private var usersList: UserDTOModels
    
    init(controller: GHUserListViewControllerProtocol,
         delegate: GHUserListPresenterDelegate,
         apiManager: APIManager = APIManager(),
         usersList: UserDTOModels) {
        self.controller = controller
        self.apiManager = apiManager
        self.usersList = usersList
        self.delegate = delegate
        self.controller?.presenter = self
    }
}

//MARK: - GHUserListPresenterProtocol
extension GHUserListPresenter: GHUserListPresenterProtocol {
    func onViewDidLoad() {
        showList() 
    }
    
    func onSelectProfile(with profileModel: UserUIModel) {
        guard let selectedModel = usersList.first(where: { $0.login == profileModel.userName }) else { return }
        
        delegate?.presenter(self, didSelect: selectedModel)
    }
    
    func showList() {
        let models = self.usersList
        self.controller?.hideLoadingController()
        let modelsForUI = UserAdapter.convertArrayToUI(models)
        controller?.show(rows: modelsForUI)
    }
}

//MARK: - Private methods
private extension GHUserListPresenter {
    func handle(error: NSError) {
        self.controller?.hideLoadingController()
        self.controller?.show(error: error)
    }
}
